define([
  "domReady",
  'jquery',
  'jquery/datepicker',
  'jquery/mask',
], function(domReady, $) {
  "use strict";

  domReady(function () {

    setTimeout(function () {
      $(".js-preload").addClass("active");
      $("html, body").addClass("active");
    }, 1000);

  });

  $(function () {

    $("#datepicker").datepicker();

    $("#mask").mask('+7 (999) 999-99-99');

  });

});
